"""
This file contains generic utility functions
"""
import os
import re
import shutil
import validators
import string

from xpresso.ai.core.commons.utils.constants import ENV_XPRESSO_PACKAGE_PATH, \
    DEFAULT_XPRESSO_PACKAGE_PATH, \
    DEFAULT_XPRESSO_CONFIG_PATH, ENV_XPRESSO_CONFIG_PATH, \
    PASSWORD_ENCRYPTION_KEY


def get_base_pkg_location():
    """ Gets the xpresso base package location from the environment variable.
    If environment variable is not found, then return default value

    Returns:
        str: xpresso base package location
    """
    return os.environ.get(ENV_XPRESSO_PACKAGE_PATH,
                          DEFAULT_XPRESSO_PACKAGE_PATH)


def get_default_config_path():
    """ Gets the default config path location using environment variable.
    If no environment variable is present, then return default config

    Returns:
        str: config_path
    """
    return os.environ.get(ENV_XPRESSO_CONFIG_PATH,
                          DEFAULT_XPRESSO_CONFIG_PATH)


def get_version():
    """
    Fetches client and api_server versions and prints out in the stdout

    Returns:
        str: version string of the project
    """
    try:
        version_file_name = os.path.join(get_base_pkg_location(), 'VERSION')
        version_fs = open(version_file_name)
        version = version_fs.read().strip()
        version_fs.close()
    except FileNotFoundError:
        # Using default version
        version = '-1'
    return version


def check_if_valid_ipv4_address(ip_address: str) -> bool:
    """ Checks if the ip address is valid IPv4 address"""
    is_valid = validators.ipv4(ip_address)
    if is_valid:
        return True
    return False


def check_if_valid_ipv6_address(ip_address: str) -> bool:
    """ Checks if the ip address is valid IPv6 address"""
    is_valid = validators.ipv6(ip_address)
    if is_valid:
        return True
    return False


def check_if_valid_dns_name(dns_name) -> bool:
    """ Checks if the dns name (fqdn) is valid """
    dns_name = dns_name.strip()
    is_valid = validators.domain(dns_name)
    if is_valid or dns_name == 'localhost':
        return True
    return False


def str2bool(string: str):
    """ Convert string to bool  value
    Args:
        string(str): string to convert to bool
    Returns:
        bool: True, if string is either true/True else False
    """
    if not string:
        return False
    true_set = {"true", "True"}
    return string.lower() in true_set


def encrypt_string(pwd: str) -> str:
    """ Encrypt string
    Args:
        pwd(str): string needed to be encrypted

    Returns:
        str: encrypted string
    """
    from cryptography.fernet import Fernet  # This is intentional

    cipher_suite = Fernet(PASSWORD_ENCRYPTION_KEY)
    encoded_text = cipher_suite.encrypt(pwd.encode("utf-8"))
    return encoded_text.decode("utf-8")


def decrypt_string(pwd: str) -> str:
    """ Decrypt string. Only those string which are envrypter using
    encrypt_string function can only be decrypted. In case of error, return
    existing pwd
    Args:
        pwd(str): string needed to be encrypted
    Returns:
        str: decrypted string
    """
    import cryptography
    from cryptography.fernet import Fernet  # This is intentional
    try:

        cipher_suite = Fernet(PASSWORD_ENCRYPTION_KEY)
        encoded_text = cipher_suite.decrypt(pwd.encode("utf-8"))
        return encoded_text.decode("utf-8")
    except (cryptography.fernet.InvalidSignature,
            cryptography.fernet.InvalidToken):
        return pwd


def convert_to_title(base_str: str) -> str:
    """ Convert the string into a title"""
    return re.sub("[^A-Za-z0-9]+", '', base_str.title())


def move_file(source, destination, overwrite=False):
    """
    Move a file from source path to destination path
    Args:
        source: Path to the source file
        destination: Path to the destination
        overwrite: Overwrites file in destination if True
    Returns:
        True if file moved successfully
    """
    if overwrite:
        file_name = os.path.basename(source)
        destination_file = os.path.join(destination, file_name)
        if os.path.exists(destination_file):
            os.remove(destination_file)
    if not os.path.exists(source):
        print("Source path missing")
        return False
    try:
        shutil.move(source, destination)
        return True
    except OSError:
        print("File exists in destination path")
        return False


def remove_keys_from_dict(dictionary: dict, key_list: list):
    """
    Removes list of keys from the dictinoary
    Args:
        dictionary:  target dictionary
        key_list: list of keys to delete
    """
    for key in key_list:
        if key in dictionary:
            dictionary.pop(key)


def remove_keys_from_list(target_list: list, key_list: list):
    """
    Removes list of keys from the list
    Args:
        target_list:  target list
        key_list: list of keys to delete
    """
    for key in key_list:
        if key in target_list:
            target_list.remove(key)
